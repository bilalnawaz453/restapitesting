describe('Rest API Testing', () => {
  
  context('Scenario 1 - GET ALL List of users', () => {
      it('should return a list with all Users', () => {
          cy.request({
              method: 'GET',
              url: 'https://reqres.in/api/users?page=2'
          })
              .then((response) => {
                expect(response.status).to.eq(200)
              expect(response.body.total).to.eq(12)
              
                  cy.log(JSON.stringify(response.body))
              });
      });
  });

  context('Scenario 2 - GET Single user', () => {
    it('should return a dataset of single User', () => {
        cy.request({
            method: 'GET',
            url: 'https://reqres.in/api/users/2'
        })
            .then((response) => {
              expect(response.status).to.eq(200)
                cy.log(JSON.stringify(response.body))
            });
    });
    
});



context('Scenario 3 - Single user data not found', () => {
  it('User data not found and return 404 response code', () => {
    cy.request({
      method: 'GET',
      url: 'https://reqres.in/api/users/23',failOnStatusCode: false,
  })
      .then((response) => {
        expect(response.status).to.eq(404)
                  
      });
  });
  });

context('Scenario 4 - GET all resource list', () => {
    it('should return all resource List', () => {
        cy.request({
            method: 'GET',
            url: 'https://reqres.in/api/unknown'
        })
            .then((response) => {
              expect(response.status).to.eq(200)
              expect(response.body.total).to.eq(12)
                cy.log(JSON.stringify(response.body))
            });
    });
    
});


context('Scenario 5 - GET single resource ', () => {
  it('should return single resource list ', () => {
      cy.request({
          method: 'GET',
          url: 'https://reqres.in/api/unknown/2'
      })
          .then((response) => {
            expect(response.status).to.eq(200)
            
              cy.log(JSON.stringify(response.body))
          });
  });
  
  
});




context('Scenario 6 -Resource list not found ', () => {
  it('should return 404 response error message', () => {
      cy.request({
          method: 'GET',
          url: 'https://reqres.in/api/unknown/23',failOnStatusCode: false,
      })
          .then((response) => {
            expect(response.status).to.eq(404)
            
              cy.log(JSON.stringify(response.body))
          });
  });
  
  
});



context('Scenario 7 - Post Request', () => {
  it('should return 201 message and has led to the creation of a resource', () => {
      cy.request({
          method: 'Post',
          url: 'https://reqres.in/api/users',
       
          body: {
            'name': 'morpheus',
            'job': 'leader'

          },
          Headers: {
            'content-type':'application/json'
          }
      })
          .then((response) => {
            expect(response.status).to.eq(201)
             cy.log(JSON.stringify(response.body))
          });
  });
  
  });

  context('Scenario 8 - PUT Request', () => {
  it('Update a user job role using Put request', () => {
    cy.request('PUT','https://reqres.in/api/users/2',{"name": "morpheus",
    "job": "zion resident"}).then((response) => {
      expect(response.status).to.eq(200)

      cy.log(JSON.stringify(response.body))
  });

  });
  });


  context('Scenario 9 - Patch Request', () => {
    it('Update a  user job role using Patch request', () => {
      cy.request('Patch','https://reqres.in/api/users/2',{"name": "morpheus",
      "job": "zion resident"}).then((response) => {
        expect(response.status).to.eq(200)

        cy.log(JSON.stringify(response.body))
    });
  
    });
    });
  

    
  context('Scenario 10 - Delete Request', () => {
    it('Delete a user by using delete command', () => {
      cy.request({
        method: 'DELETE',
        url: 'https://reqres.in/api/users?page=2'
    })
        .then((response) => {
          expect(response.status).to.eq(204)
          
            
        });
    });
    });

    

context('Scenario 11 - User Registration Successfull', () => {
  it('should be return status code 200 with id and token in response', () => {
      cy.request({
          method: 'Post',
          url: 'https://reqres.in/api/register',
          Headers:
          {
            'content-type' : 'applicaton/json'
          },
          body: {
            'email': 'eve.holt@reqres.in',
            'password': 'pistol'

          }
        
      })
          .then((response) => {
            expect(response.status).to.eq(200)
             cy.log(JSON.stringify(response.body))
          });
  });
  
  });



  context('Scenario 12 - User Registration unsuccessfull', () => {
    it('should be return 400 error code with error message', () => {
        cy.request({
            method: 'Post',
            url: 'https://reqres.in/api/register',failOnStatusCode: false,
            Headers:
            {
              'content-type' : 'applicaton/json'
            },
            body: {
              'email': 'sydney@fife'
  
            }
          
        })
            .then((response) => {
              expect(response.status).to.eq(400)
               cy.log(JSON.stringify(response.body))
            });
    });
    
    });
  



    
  context('Scenario 13 - Login Successfull', () => {
    it('should be return 200 code with token in response body', () => {
        cy.request({
            method: 'Post',
            url: 'https://reqres.in/api/login',failOnStatusCode: false,
            Headers:
            {
              'content-type' : 'applicaton/json'
            },
            body: {
              'email': 'eve.holt@reqres.in'
  
            }
          
        })
            .then((response) => {
              expect(response.status).to.eq(400)
               cy.log(JSON.stringify(response.body))
            });
    });
    
    });


    
    context('Scenario 14 - Login Unsuccessfull', () => {
      it('should be return 400 error code with error mesage in response', () => {
          cy.request({
              method: 'Post',
              url: 'https://reqres.in/api/login',
              Headers:
              {
                'content-type' : 'applicaton/json'
              },
              body: {
                'email': 'eve.holt@reqres.in',
                'password': 'cityslicka'
    
              }
            
          })
              .then((response) => {
                expect(response.status).to.eq(200)
                 cy.log(JSON.stringify(response.body))
              });
      });
      
      });
  

      context('Scenario 15 - Delayed Response', () => {
        it('should be return list of user with delayed response', () => {
            cy.request({
                method: 'GET',
                url: 'https://reqres.in/api/users?delay=3'
            })
                .then((response) => {
                  expect(response.status).to.eq(200)
                    cy.log(JSON.stringify(response.body))
                });
        });
        
    });
    


});